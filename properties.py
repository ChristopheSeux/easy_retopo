import bpy

OP_to_snap = ('MESH_OT_f2',
'MESH_OT_loopcut_slide',
'TRANSFORM_OT_edge_slide',
'TRANSFORM_OT_vert_slide',
'MESH_OT_vertices_smooth',
'MESH_OT_bevel',
'MESH_OT_fill_grid',
'MESH_OT_bridge_edge_loops',
'MESH_OT_subdivide',
)

OP_selection = ('VIEW3D_OT_select_circle',
'VIEW3D_OT_select_border',
'VIEW3D_OT_select_lasso',
'VIEW3D_OT_select',
'MESH_OT_select'
)


view_settings = {'addons':{'mesh_f2':1},
            'tool_settings':{
                    "use_snap":1,
                    "snap_element":'FACE',
                    "use_snap_project":1,
                    "mesh_select_mode":(True, True, False)
                    },
            'space_data':{
                    "show_occlude_wire":1,
                    "show_floor":0,
                    "show_axis_x":0,
                    "show_axis_y":0,
                    "show_axis_z":0
                    }
            }


def enable_snap(self,context):
    context.scene.tool_settings.use_snap = self.auto_snap

class Settings(bpy.types.PropertyGroup):
    face_color = bpy.props.FloatVectorProperty(name="Face",subtype='COLOR', default=[0.6,0.88,1.0,0.2],size = 4,soft_min=0,soft_max=1)
    edge_color = bpy.props.FloatVectorProperty(name="Edge",subtype='COLOR', default=[0.06,0.145,0.2],soft_min=0,soft_max=1)
    vert_color = bpy.props.FloatVectorProperty(name="Vert",subtype='COLOR', default=[0.01,0.015,0.02],soft_min=0,soft_max=1)
    selected_color = bpy.props.FloatVectorProperty(name="Selected",subtype='COLOR', default=[0.9,0.9,0.0],soft_min=0,soft_max=1)

    offset = bpy.props.FloatProperty(default = 1.0)

    auto_snap = bpy.props.BoolProperty(default = True,update=enable_snap)
