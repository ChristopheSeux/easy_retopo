import bpy
import bmesh
from mathutils import Matrix,Vector
from math import radians,atan2
import numpy as np
from bpy_extras.view3d_utils import region_2d_to_vector_3d as vector_from_2d
from bpy_extras.view3d_utils import region_2d_to_origin_3d as org_from_2d
from bpy_extras.view3d_utils import location_3d_to_region_2d as loc_from_3d
from bpy_extras.view3d_utils import region_2d_to_location_3d as loc_from_2d


def unregister_keymaps(addon_keymaps) :
    wm = bpy.context.window_manager
    for km in addon_keymaps:
        for kmi in km.keymap_items:
            km.keymap_items.remove(kmi)

def register_keymaps() :
    # Selection
    addon = bpy.context.window_manager.keyconfigs.addon
    addon_keymaps = []

    # Keymaps Mesh
    mesh_op = [{"id" :"mesh.vertex_slide","type" : "G","ctrl":False,"alt":True},
    {"id" :"mesh.circle_slice","type" : "C","ctrl":True,"alt":True},
    {"id" :"mesh.vertices_smooth","type" : "W","ctrl":False,"alt":True}]

    for op_info in mesh_op :
        km = addon.keymaps.new(name = '3D View', space_type = 'VIEW_3D')
        km.keymap_items.new(op_info['id'], type = op_info['type'], value = "PRESS",ctrl =op_info['ctrl'] ,alt=op_info['alt'])
        addon_keymaps.append(km)


    select_op = [{"op_type":"select","type" : "SELECTMOUSE","ctrl":False,"shift":False},
    {"op_type":"select_border","type" : "B","ctrl":False,"shift":False},
    {"op_type":"select_circle","type" : "C","ctrl":False,"shift":False},
    {"op_type":"select_lasso","type" : "ACTIONMOUSE","ctrl":True,"shift":False,'deselect' : False},
    {"op_type":"select_lasso","type" : "ACTIONMOUSE","ctrl":True,"shift":True,'deselect' : True}]

    for op_info in select_op :
        km = addon.keymaps.new(name = '3D View', space_type = 'VIEW_3D')
        kmi = km.keymap_items.new("mesh.select", type = op_info["type"], value = "PRESS",ctrl =op_info['ctrl'] ,shift=op_info['shift'])
        kmi.properties.op_type = op_info['op_type']
        if op_info['op_type'] == 'select_lasso' :
            kmi.map_type ='TWEAK'
            kmi.properties.deselect = op_info['deselect']

        addon_keymaps.append(km)


    return addon_keymaps



def set_view_settings(space_data,settings):
    import copy
    import addon_utils

    stored_settings = copy.deepcopy(settings)
    scene = bpy.context.scene

    for addon,check in settings['addons'].items() :
        stored_settings['addons'][addon] = addon_utils.check(addon)[0]
        if not addon_utils.check(addon)  :
            addon_utils.ensable(addon)

    for attr,value in settings['tool_settings'].items() :
        current_value = getattr(scene.tool_settings,attr)
        if isinstance(current_value,bpy.types.bpy_prop_array) :
            current_value = tuple(current_value)
            value = tuple([bool(v) for v in value])
        stored_settings['tool_settings'][attr] = current_value

        setattr(scene.tool_settings,attr,value)

    for attr,value in settings["space_data"].items() :
        stored_settings['space_data'][attr] = getattr(space_data,attr)
        setattr(space_data,attr,value)

    return stored_settings


def reproject(ob,verts,mode) :
    if not verts :
        return

    center = ob.matrix_world*Vector(np.mean([v.co for v in verts],axis=0))

    if mode == 'NORMAL' :
        vert_info = [(v,v.normal.copy()) if v.link_faces else (v,ob.matrix_world*v.co-center) for v in verts ]
    elif mode == 'CENTER' :
        vert_info = [(v,ob.matrix_world*v.co-center) for v in verts]

    for vert,axis in vert_info :
        result = None
        co = ob.matrix_world*vert.co.copy()

        vert.co = ob.matrix_world.inverted()*co - axis.normalized()*0.005
        ob.data.update()
        raycast_cast = bpy.context.scene.ray_cast(co,axis)

        vert.co = ob.matrix_world.inverted()*co + axis.normalized()*0.005
        ob.data.update()
        inverse_ray_cast = bpy.context.scene.ray_cast(co,axis*-1)


        if raycast_cast[0] :
            if not inverse_ray_cast :
                result = raycast_cast[1]

            else  :
                distance = (raycast_cast[1]-co).length
                inverse_distance = (inverse_ray_cast[1]-co).length

                if distance<inverse_distance :
                    result = raycast_cast[1]
                else :
                    result = inverse_ray_cast[1]

        elif inverse_ray_cast[0] :
            result = inverse_ray_cast[1]

        if result :
            vert.co = ob.matrix_world.inverted()*result

    bmesh.update_edit_mesh(ob.data)
    ob.data.update()



def create_circle(region,rv3d,bm,segments,mouse_start,mouse_end) :
    scene = bpy.context.scene
    ob = bpy.context.object

    view_matrix = rv3d.view_matrix.inverted()

    view_vector = vector_from_2d(region, rv3d, mouse_start)
    ray_origin = org_from_2d(region, rv3d, mouse_start)

    hit,start_loc,normal,index,object,matrix = scene.ray_cast(ray_origin,view_vector)

    #return
    if not hit :
        print('no raycast1')
        return

    hit,end_loc,normal,index,object,matrix = scene.ray_cast(start_loc+0.001*view_vector.normalized(),view_vector)
    if not hit :
        print('no raycast2')
        return

    mouse_vector = mouse_end-mouse_start
    dot = 0*mouse_vector[0] + 1*mouse_vector[1]     # dot product between [x1, y1] and [x2, y2]
    det = 0*mouse_vector[1] - 1*mouse_vector[0]     # determinant
    angle = atan2(det, dot)

    rot_matrix = Matrix.Rotation(radians(90.0), 4, 'Y').to_3x3()

    view_rot_matrix = Matrix.Rotation(-angle, 4, 'X').to_3x3()

    view_matrix = view_matrix.to_3x3()

    center = (start_loc+end_loc)/2

    loc_mat = Matrix.Translation((start_loc+end_loc)/2)
    rot_mat = view_matrix*rot_matrix *view_rot_matrix

    mouse_end_3d = loc_from_2d(region, rv3d, mouse_end ,center)
    radius = (center-mouse_end_3d).length

    transform_mat = loc_mat*rot_mat.to_4x4()

    verts_info = bmesh.ops.create_circle(bm,cap_ends=False,diameter=radius,segments=segments,matrix = transform_mat)
    bmesh.update_edit_mesh(ob.data)

    return bm

# used for circle slice
def shape_bridge(bm,shape) :
    ob = bpy.context.object
    selected_edges = [e for e in bm.edges if e.select]
    iteration = 5

    verts = []
    for vert in shape.verts :
        verts.append(bm.verts.new(ob.matrix_world.inverted()*vert.co))

    bmesh.update_edit_mesh(ob.data)

    new_edges =[]
    for edge in shape.edges :
        new_edge = bm.edges.new([verts[v.index] for v in edge.verts])
        new_edges.append(new_edge)

    for e in new_edges :
        e.select = True

    reproject(ob,verts,'CENTER')



    for i in range(iteration) :
        bmesh.ops.smooth_vert(bm, verts = verts, factor = 0.5,use_axis_x=1,use_axis_y=1,use_axis_z=1)
        bmesh.update_edit_mesh(ob.data)
        reproject(ob,verts,'CENTER')

    if selected_edges :
        bpy.ops.mesh.bridge_edge_loops()

    bpy.ops.mesh.select_all(action='DESELECT')

    for e in new_edges :
        e.select = True
