import bpy
from bgl import *
from .functions import *
from .utils import *
from .properties import OP_to_snap,OP_selection,view_settings
import os
from os.path import join,exists,basename,splitext
import json
import time
import tempfile


_handle = None
_space_data = None

def draw_callback_px(self, context):

    scene = context.scene
    settings = scene.easy_retopo
    #prefs = bpy.context.user_preferences.addons[__name__].preferences
    operators = context.window_manager.operators
    ob = context.object

    if not ob or ob.mode!= 'EDIT' :
        return

    edge_size = 1.5
    point_size = 5

    offset = settings.offset


    if not self.bm.is_valid :
        if len(operators) and operators[-1].bl_idname in OP_to_snap and settings.auto_snap:
            reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')
            #ob.data.update()
        self.bm = bmesh.from_edit_mesh(ob.data)


    bm = self.bm

    region = context.region
    rv3d = context.region_data

    world_co = [ob.matrix_world *v.co for v in  bm.verts]
    screen_co = [loc_from_3d(region, rv3d, co) for co in world_co]

    #origins = [org_from_2d(region, rv3d, co_2d) for co_2d in screen_co]
    #vectors = [vector_from_2d(region, rv3d, co_2d) for co_2d in origins]


    screen_offset = [co_3d-(vector_from_2d(region, rv3d,co_2d))*0.1*offset if co_2d else co_3d for co_3d,co_2d in zip(world_co,screen_co)]


    face_color = {0:settings.face_color,1:[*settings.selected_color,settings.face_color[-1]]}
    edge_color = {0:settings.edge_color,1:settings.selected_color*1.05}
    vert_color = {0:settings.vert_color,1:settings.selected_color*1.15}


    glEnable(GL_BLEND)
    glEnable( GL_LINE_SMOOTH )
    glEnable(GL_CULL_FACE)
    glLineWidth(edge_size)
    glPointSize(point_size)

    glDepthMask(GL_FALSE)

    for i,face in enumerate(bm.faces) :
        #glColor4f(*face_color[i])
        glColor4f(*face_color[face.select])

        glBegin(GL_POLYGON)
        for vert in face.verts :
            glVertex3f(*screen_offset[vert.index])
        glEnd()


    glBegin(GL_LINES)
    for i,edge in enumerate(bm.edges) :
        #glColor3f(*edge_color[i])
        glColor3f(*edge_color[edge.select])

        for vert in edge.verts :
            glVertex3f(*screen_offset[vert.index])
    glEnd()

    #glDepthRange (0.02, 1.0)
    #glDepthRange (0.0, 1.0)
    #glClear(GL_DEPTH_BUFFER_BIT)
    #glDepthRange (0.0,0.998)

    glBegin(GL_POINTS)
    for vert in bm.verts :
        #glColor3f(*vert_color[diff[vert.index]])
        glColor3f(*vert_color[vert.select])
        glVertex3f(*screen_offset[vert.index])

    glEnd()

    # restore opengl defaults
    glLineWidth(1)
    glPointSize(1)
    glDisable(GL_BLEND)
    glDisable( GL_LINE_SMOOTH )
    glColor4f(0.0, 0.0, 0.0, 1.0)
    #glDepthRange(0.001, 1.0)

    glEnable(GL_DEPTH_TEST)
    #glDepthRange (offset*factor*10, 1.0)
    glDisable(GL_CULL_FACE)
    #glDepthFunc(GL_LESS)
    glDepthMask(GL_TRUE)
    glDisable( GL_POLYGON_OFFSET_FILL )

    #context.area.tag_redraw()
class StartRetopo(bpy.types.Operator):
    """Starting the retopology process"""
    bl_idname = "mesh.start_retopo"
    bl_label = "Start Retopology"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        return context.area.type == 'VIEW_3D'

    def is_inside_area(self,area,mouse) :
        if 0 < mouse[0]-area.x < area.width and 0 < mouse[1]-area.y < area.height:
            return True


    def modal(self, context, event):
        ob = context.object
        scene = context.scene
        settings = scene.easy_retopo
        wm = context.window_manager
        global _handle
        #prefs = bpy.context.user_preferences.addons[__name__].preferences
        region = context.region
        rv3d = context.region_data

        # Exit the modal operator
        if not ob or ob.mode!= 'EDIT' or not _handle:
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            #self.bm.free()
            set_view_settings(_space_data,scene["view_settings"].to_dict())

            del scene["view_settings"]
            _handle = None

            wm.event_timer_remove(self._timer)

            unregister_keymaps(self.addon_keymaps)

            return {'CANCELLED'}


        if not hasattr(context.area,"tag_redraw") or context.area.type!='VIEW_3D' :
            return {'PASS_THROUGH'}

        context.area.tag_redraw()

        # backup
        if time.time() - self.timer > 20 :
            #print('backup')
            filename = basename(bpy.data.filepath) if bpy.data.filepath else 'untitled'
            filepath = join(self.temp_dir,splitext(filename)[0]+'.json')

            data = mesh_to_dict(bmesh.from_edit_mesh(ob.data))
            with open(filepath, 'w') as outfile:
                json.dump(data, outfile)

            self.timer = time.time()



        if self.is_inside_area(context.area.regions[4],(event.mouse_x,event.mouse_y)) :
            if event.ctrl and not self.ctrl:
                self.ctrl = True
                self.manipul = context.space_data.show_manipulator
                context.space_data.show_manipulator = False

            if self.ctrl and not event.ctrl :
                self.ctrl = False
                context.space_data.show_manipulator = self.manipul

                if settings.auto_snap :
                    reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')

        if self.bm.is_valid :
            self.bm.normal_update()


        operators = wm.operators

        if len(wm.operators) :
            if self.operators != [o.bl_idname for o in wm.operators] or self.redo_last or operators[-1].is_repeat():
                if  settings.auto_snap and operators[-1].bl_idname in OP_to_snap :
                    reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')

                    self.redo_last = False

                elif operators[-1].bl_idname in OP_selection :
                    reveal_mesh_component()

                self.operators = [o.bl_idname for o in wm.operators]

        redo_last = context.window_manager.keyconfigs.active.keymaps["Screen"].keymap_items['screen.repeat_last']
        if key_map_pressed(event,redo_last) :
            self.redo_last = True




        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        scene = context.scene
        ob = context.object
        wm = context.window_manager

        self.operators_len = len(context.window_manager.operators)
        self.operators = [o.bl_idname for o in context.window_manager.operators]
        self.ctrl = False
        self.manipul = False
        self.redo_last = False
        self.screen_offset = []
        self.visibility = []
        self.timer = time.time()


        if ob.mode != 'EDIT' :
            mesh = bpy.data.meshes.new(ob.data.name+'_retopo')
            ob = bpy.data.objects.new(ob.name+'_retopo',mesh)

            bpy.context.scene.objects.link(ob)
            ob.matrix_world = ob.matrix_world
            scene.objects.active = ob

            #deselect all objects :
            for selected_ob in context.selected_objects :
                selected_ob.select = False

            bpy.ops.object.mode_set(mode='EDIT')


        self.bm = bmesh.from_edit_mesh(ob.data)

        if not scene.get("view_settings") :
            scene["view_settings"] = set_view_settings(context.space_data,view_settings)

        args = (self, context)
        self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_VIEW')
        wm.modal_handler_add(self)

        global _handle
        global _space_data

        _handle = self._handle
        _space_data = context.space_data

        self._timer = wm.event_timer_add(160, context.window)

        # create temp dir
        temp_dir = tempfile.gettempdir()
        self.temp_dir = join(temp_dir,'easy_retopo')
        if not exists(self.temp_dir) :
            os.mkdir(self.temp_dir)


        self.addon_keymaps = register_keymaps()

        bpy.ops.ed.undo_push()
        return {'RUNNING_MODAL'}
