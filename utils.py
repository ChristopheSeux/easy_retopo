import bpy
import bmesh
from bpy_extras.view3d_utils import region_2d_to_vector_3d as vector_from_2d
from bpy_extras.view3d_utils import region_2d_to_origin_3d as org_from_2d
from bpy_extras.view3d_utils import location_3d_to_region_2d as loc_from_3d


def key_map_pressed(event,kmi) :
    conditions = [event.type == kmi.type,
                event.shift==  kmi.shift,
                event.alt==  kmi.alt,
                event.oskey==  kmi.oskey,
                event.value==  kmi.value]

    return all(conditions)


def reveal_mesh_component():
    ob = bpy.context.object
    bm = bmesh.from_edit_mesh(ob.data)

    #print('reveal')

    for vert in bm.verts :
        vert.hide = False

    for edge in bm.edges :
        edge.hide = False

        if all([v.select for v in edge.verts]) :
            edge.select = True

    for face in bm.faces :
        face.hide = False
        if all([v.select for v in face.verts]) :
            face.select = True

def hide_mesh_component(offset):
    scene = bpy.context.scene
    ob = bpy.context.object
    bm = bmesh.from_edit_mesh(ob.data)

    region = bpy.context.region
    rv3d = bpy.context.region_data

    world_co = [ob.matrix_world *v.co for v in  bm.verts]
    screen_co = [loc_from_3d(region, rv3d, co) for co in world_co]

    origins = [org_from_2d(region, rv3d, co_2d) for co_2d in screen_co]
    vectors = [vector_from_2d(region, rv3d, co_2d) for co_2d in origins]

    screen_offset = [co_3d-vec*0.1*offset for co_3d,vec in zip(world_co,vectors)]

    casts = [scene.ray_cast(org,vector_from_2d(region, rv3d, co))[1] for org,co in zip(origins,screen_co)]
    depth = [(co-org).length for org,co in zip(origins,screen_offset)]

    cast_depth = [(loc-org).length for org,loc in zip(origins,casts)]

    diff = [d<=c for d,c in zip(depth,cast_depth)]

    for i,vert in enumerate(bm.verts) :
        vert.hide = not diff[i]

    for edge in bm.edges :
        edge.hide = any([v.hide for v in edge.verts])

    for face in bm.faces :
        face.hide = all([v.hide for v in face.verts])


def mesh_to_dict(bm) :
    info = {}

    verts = [v.co[:] for v in bm.verts]
    edges = [[v.index for v in e.verts] for e in bm.edges]
    edge_key = [[e.index for e in f.edges] for f in bm.faces]
    edge_key_flat = [e for face in edge_key for e in face]

    info['verts'] = verts
    info['edges'] = [e for e in edges if e not in edge_key_flat]
    info['faces'] = [[v.index for v in f.verts] for f in bm.faces]

    return info
