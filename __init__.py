bl_info = {
    "name": "Easy Retopo",
    "author": "Christophe Seux",
    "version": (1, 0),
    "blender": (2, 79, 0),
    "location": "View3D >Tools>Retopology",
    "description": "",
    "warning": "",
    "wiki_url": "",
    "category": "Mesh",
    }


if "bpy" in locals():
    import importlib
    importlib.reload(operators)
    importlib.reload(OP_circle_slice)
    importlib.reload(OP_start_retopo)
    importlib.reload(panels)
    importlib.reload(functions)
    importlib.reload(properties)

else :
    from . import operators
    from . import properties
    from . import OP_circle_slice
    from . import OP_start_retopo
    from . import panels
    from . functions import *



import bpy
from bpy.app.handlers import persistent

@persistent
def restore_settings(dummy):
    scene = bpy.context.scene
    if scene.get("view_settings"):
        restore_settings.view_settings = set_view_settings(_space_data,scene["view_settings"].to_dict())


@persistent
def reapply_settings(dummy):
    scene = bpy.context.scene
    if scene.get("view_settings"):
        view_settings = set_view_settings(_space_data,restore_settings.view_settings)




def register():
    wm = bpy.context.window_manager
    addon = bpy.context.window_manager.keyconfigs.addon

    bpy.utils.register_module(__name__)

    bpy.types.Scene.easy_retopo = bpy.props.PointerProperty(type = properties.Settings)

    # Handler registration
    bpy.app.handlers.save_pre.append(restore_settings)
    bpy.app.handlers.save_post.append(reapply_settings)



def unregister():
    bpy.app.handlers.save_pre.remove(restore_settings)
    bpy.app.handlers.save_post.remove(reapply_settings)

    del bpy.types.Scene.easy_retopo


    bpy.utils.register_module(__name__)
