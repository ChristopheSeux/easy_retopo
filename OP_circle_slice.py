import bpy
from bgl import *
import blf
from .functions import *
from .utils import *
from mathutils import Vector


from bpy_extras.view3d_utils import location_3d_to_region_2d as loc_from_3d


def draw_circle_slice(self,context) :
    bm = bmesh.new()
    bmesh.ops.create_circle(bm,cap_ends = False,segments = 12,diameter=5)

    glEnable(GL_BLEND)
    glEnable( GL_LINE_SMOOTH )
    glColor4f(0,0,0,0.75)
    glLineWidth(2)

    if self.start_slicing :
        points = [loc_from_3d(context.region, context.region_data, v.co) for v in self.circle.verts]
        glColor4f(0.9,0.9,0.9,0.25)
        glBegin(GL_LINE_LOOP)
        for p in points :
           glVertex2f(*p)

        glEnd()

        glPointSize(5)
        glBegin(GL_POINTS)
        for p in points :
            glVertex2f(*p)

        glEnd()


    glColor4f(0,0,0,0.75)
    glBegin(GL_LINE_LOOP)
    for vert in bm.verts :
        coord = self.mouse+Vector((vert.co[0],vert.co[1]))
        glVertex2f(*coord)

    glEnd()

    font_id = 0


    # draw some text
    blf.position(font_id, self.mouse[0]+10, self.mouse[1]+5, 0)
    blf.size(font_id, 14, 72)

    #blf.blur(font_id, 2)
    #glColor4f(1.0,1.0,1.0,0.75)
    #blf.draw(font_id, "8")

    #blf.blur(font_id, 0)
    glColor4f(0.0,0.0,0.0,1.0)
    blf.draw(font_id, str(self.segments))

    bm.free()

    glDisable(GL_BLEND)
    glDisable(GL_LINE_STIPPLE)
    glDisable(GL_LINE_SMOOTH)
    glLineWidth(1)
    glPointSize(1)


class CircleSlice(bpy.types.Operator):
    """Add a circle around the mesh"""
    bl_idname = "mesh.circle_slice"
    bl_label = "Circle Slice"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        ob = context.object
        return context.area.type == 'VIEW_3D' and ob and ob.mode == 'EDIT'

    def modal(self, context, event):
        ob = context.object
        region = context.region
        rv3d = context.region_data

        context.area.tag_redraw()

        self.mouse = Vector((event.mouse_region_x,event.mouse_region_y))

        if event.type in {'ESC','RIGHTMOUSE'}:
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'CANCELLED'}




        elif event.type == "WHEELDOWNMOUSE" :
            self.segments -= 1

        elif event.type == "WHEELUPMOUSE" :
            self.segments += 1

        elif event.type == 'LEFTMOUSE' :
            if event.value == 'PRESS' :
                self.start_slicing = True
                self.mouse_start = Vector((event.mouse_region_x,event.mouse_region_y))
                #print(self.mouse_start)

            if event.value == 'RELEASE' :
                #print('release')

                self.start_slicing = False
                self.mouse_end = Vector((event.mouse_region_x,event.mouse_region_y))

                shape_bridge(self.bm,self.circle)

                bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
                return {'FINISHED'}

        if self.start_slicing :
            self.circle = create_circle(region,rv3d,bmesh.new(),self.segments,self.mouse_start,self.mouse)


        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.start_slicing = False
        self.mouse = Vector((0,0))
        self.mouse_start = Vector((0,0))
        self.mouse_end= Vector((0,0))
        self.circle = None


        self.bm = bmesh.from_edit_mesh(context.object.data)

        selected_edges = [e for e in self.bm.edges if e.select]
        self.segments = len(selected_edges) if selected_edges else 8


        args = (self, context)
        self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_circle_slice, args, 'WINDOW', 'POST_PIXEL')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}
