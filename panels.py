import bpy
from . import OP_start_retopo

class Panel(bpy.types.Panel) :
    bl_space_type = 'VIEW_3D'
    bl_label = "Retopology"
    bl_region_type = 'TOOLS'
    bl_category = 'Tools'

    @classmethod
    def poll(self,context) :
        return context.object

    def draw(self,context) :
        layout = self.layout
        scene = context.scene
        settings = scene.easy_retopo
        ob = context.object

        text = "Continue Retopo" if ob.mode == 'EDIT' else "Start Retopo"
        if OP_start_retopo._handle :
            layout.operator("mesh.exit_retopo",icon= 'X',text = 'Exit Retopo')
        layout.operator("mesh.start_retopo",icon= 'MOD_TRIANGULATE',text = text)

        col = layout.column(align = True)

        if ob and ob.mode =='EDIT' :
            col.operator("mesh.reproject",icon='RETOPO')
            col.prop(settings,"auto_snap",icon='SNAP_ON')

            col.separator()
            col.operator("mesh.circle_slice",icon= 'CURVE_NCIRCLE')
            col.operator("mesh.vertex_slide",icon='EDGESEL')

            col.separator()
            col.prop(settings,"offset")

            row = col.row()
            face_col = row.column()
            face_col.label(icon = 'FACESEL',text = "")
            face_col.prop(settings,"face_color",text = "")

            edge_col = row.column()
            edge_col.label(icon = 'EDGESEL',text = "")
            edge_col.prop(settings,"edge_color",text = "")

            vert_col = row.column()
            vert_col.label(icon = 'VERTEXSEL',text = "")
            vert_col.prop(settings,"vert_color",text = "")

            selected_col = row.column()
            selected_col.label(icon = 'SNAP_FACE',text = "")
            selected_col.prop(settings,"selected_color",text = "")

            col.separator()
            col.operator("mesh.restore_retopo")
