import bpy
from .utils import*
from .functions import *
from .import OP_start_retopo

class RestoreRetopo(bpy.types.Operator) :
    """Restore the last retopo"""
    bl_idname = "mesh.restore_retopo"
    bl_label = "Restore Retopo"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        ob = context.object
        return ob and ob.mode =='EDIT'

    def execute(self,context) :
        import json
        import os
        import tempfile
        from os.path import join,basename,splitext

        temp_dir = tempfile.gettempdir()
        ob = context.object

        back_up_dir = join(temp_dir,'easy_retopo')
        if not back_up_dir :
            print('no backup dir')
            return {"FINISHED"}

        filename = basename(bpy.data.filepath) if bpy.data.filepath else 'untitled'
        files = [join(back_up_dir,f) for f in os.listdir(back_up_dir) if splitext(f)[0] == splitext(filename)[0]]
        if not files :
            print('no backup file')
            return {"FINISHED"}

        with open(files[0]) as f:
            mesh_info = json.load(f)


        bpy.ops.object.mode_set(mode = "OBJECT")

        backup_mesh = bpy.data.meshes.new('retopo_restored')

        backup_mesh.from_pydata(mesh_info['verts'],mesh_info['edges'],mesh_info['faces'])
        backup_mesh.update(calc_edges=True)

        ob.data = backup_mesh

        bpy.ops.object.mode_set(mode = "EDIT")
        return {"FINISHED"}


class ExitRetopo(bpy.types.Operator) :
    """Exit the retopology process"""
    bl_idname = "mesh.exit_retopo"
    bl_label = "Exit Retopology"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        ob = context.object
        return context.area.type == 'VIEW_3D' and ob and ob.mode =='EDIT'

    def execute(self,context) :
        #global _handle

        OP_start_retopo._handle = None
        return {"FINISHED"}



class Reproject(bpy.types.Operator):
    """Draw a line with the mouse"""
    bl_idname = "mesh.reproject"
    bl_label = "Reproject"

    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        return context.object and context.object.mode == 'EDIT'

    def execute(self,context) :
        ob = context.object

        reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')
        ob.data.update()


        return {"FINISHED"}






class Select(bpy.types.Operator):
    """Select mesh component ignoring hiding"""
    bl_idname = "mesh.select"
    bl_label = "Select Mesh Component"
    bl_options = {'REGISTER','UNDO'}

    op_type = bpy.props.StringProperty()
    deselect = bpy.props.BoolProperty(default = False)

    @classmethod
    def poll(self,context) :
        ob = context.object
        return context.area.type == 'VIEW_3D' and ob and ob.mode == 'EDIT'

    def execute(self,context) :
        ob = context.object
        settings = context.scene.easy_retopo

        #print('real select')

        operator = getattr(bpy.ops.view3d,self.op_type)

        #bm= bmesh.from_edit_mesh(ob.data)
        hide_mesh_component(settings.offset)

        if self.deselect == True:
            operator("INVOKE_DEFAULT",deselect = True)
        else :
            operator("INVOKE_DEFAULT")

        #reveal_mesh_component()
        #bpy.ops.transform.edge_slide("INVOKE_DEFAULT",release_confirm = True)

        #reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')

        return {"FINISHED"}





class VertexSlide(bpy.types.Operator):
    """Add a circle around the mesh"""
    bl_idname = "mesh.vertex_slide"
    bl_label = "Slide"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(self,context) :
        ob = context.object
        return context.area.type == 'VIEW_3D' and ob and ob.mode == 'EDIT'


    def execute(self,context) :
        ob = context.object
        #bm= bmesh.from_edit_mesh(ob.data)
        bpy.ops.transform.edge_slide("INVOKE_DEFAULT",release_confirm = True)

        #reproject(ob,[v for v in bmesh.from_edit_mesh(ob.data).verts if v.select],'NORMAL')

        return {"FINISHED"}
